chef_ver='11.8.0'

echo "-- Check for Chef version $chef_ver"
if ( ! `chef-client --version | grep $chef_ver > /dev/null` ); then
  echo "  -- Installing chef"
  sudo apt-get install curl -y

  curl -L https://www.opscode.com/chef/install.sh | sudo bash -s -- -v $chef_ver

  source ~/.profile

  exit 0
else
  echo "  -- Chef already installed"
fi

sudo mkdir -p /etc/chef
